# Rustler CDN Index UI

## Making Changes

  1. Ensure you have Node.js and npm installed.
  2. Execute `npm start` to start a live HTTP server at 
     [http://localhost:8080](http://localhost:8080) 
     with live reload capability.
  3. Make your changes, save, and watch magic happen.

## Building with Webpack

  1. Ensure you have Node.js and npm installed.
  2. Execute `npm run build:dev` to build target site with development 
     environment options (clear scripts and CSS).
  3. Execute `npm run build:prod` to build target site with production 
     environment options (obfuscated/minified scripts and CSS). Also 
     gzips HTML and JS files for in-place compressed hosting on S3.
     You'll need to manually add a `Content-Encoding: gzip` header
     to these files in S3 in order for them to be served correctly.
  4. Find the output in the `/dist` folder.