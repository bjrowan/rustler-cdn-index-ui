const { merge } = require("webpack-merge");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpackBaseConfig = require("./webpack.common.config.js");
const webpack = require("webpack");

module.exports = merge(webpackBaseConfig, {
    mode: 'development',
    devtool: 'eval-source-map',
    plugins: [
        new CopyWebpackPlugin({
            patterns: [{
                from: "./example_json",
                to: "example_json" 
            }]
        }),
        new webpack.DefinePlugin({
            __INDEX_BASE_URI: JSON.stringify('./example_json/'),
            __CDN_BASE_URI: JSON.stringify('https://files.bjrowan.com/rustler-cdn/'),
        })
    ]
});
