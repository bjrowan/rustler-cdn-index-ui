const { merge } = require("webpack-merge");
const TerserPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const webpackBaseConfig = require("./webpack.common.config.js");
const webpack = require("webpack");

module.exports = merge(webpackBaseConfig, {
    mode: 'production',
    plugins: [
        new webpack.DefinePlugin({
            __INDEX_BASE_URI: JSON.stringify('https://files.bjrowan.com/rustler-cdn/'),
            __CDN_BASE_URI: JSON.stringify('https://files.bjrowan.com/rustler-cdn/'),
        })
    ],
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin(),
            new CssMinimizerPlugin(),
            // Statically compress certain files so they can be hosted e.g. in an S3 bucket
            // where content compression is not available from the HTTP server.
            new CompressionPlugin({
                algorithm: 'gzip',
                test: /\.(js|html)$/
            })
        ]
    }
});
