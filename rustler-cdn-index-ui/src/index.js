'use strict';

import '../src/css/index-style.css';

var CdnIndexModule = (function () {

    var module = {};

    // Private properties.
    const _indexBaseUri = __INDEX_BASE_URI;
    const _cdnBaseUri = __CDN_BASE_URI;
    const _packageInfoFileName = 'package-index.json';
    var _cdnPackageIndex;
    var _packageLinks;
    var _packageFileItems;
    var _initialPage;
    var _initialized = false;

    /**
     * Load the package info JSON.
     */
    var _loadPackageJson = function () {
        
        _showLoadingAnim(true);

        const msgErrorLoading = 'Error retrieving Rustler CDN index. Please try again later.';
        const msgErrorParsing = 'Error parsing Rustler CDN index data. Please try again later.';
        
        let loadingBar = document.getElementById('loading-progress-bar');
        let xhr = new XMLHttpRequest();
        xhr.timeout = 15000;
        xhr.open('GET', _indexBaseUri + _packageInfoFileName);
        xhr.send();

        xhr.onload = function(e) {
            if (xhr.status === 200 && xhr.response && xhr.response.length > 0) {
                try {
                    _cdnPackageIndex = JSON.parse(xhr.response);
                    _hydrateDom();
                    _prepareUI();
                }
                catch (ex) {
                    _createPopupMessage(msgErrorParsing, 'error', false);
                    console.log(ex);
                }
            }
            else {
                _createPopupMessage(msgErrorLoading, 'error', true);
            }
            _showLoadingAnim(false);
        };

        xhr.onprogress = function(e) {
            if (e.lengthComputable) {
                console.log(`Received ${e.loaded} of ${e.total} bytes`);
                loadingBar.style.width = (e.loaded / e.total).toString() + '%';
            }
        }

        xhr.onerror = function() {
            _createPopupMessage(msgErrorLoading, 'error', true);
            _showLoadingAnim(false);
        };

        xhr.ontimeout = function() {
            _createPopupMessage(msgErrorLoading, 'error', true);
            _showLoadingAnim(false);
        }
    }

    /**
     * Hydrate the DOM with elements based on the package info JSON.
     */
    var _hydrateDom = function () {
        if (_cdnPackageIndex === undefined || !_cdnPackageIndex.packages) {
            console.log('Error: Tried to hydrate the DOM without any package index JSON.');
            return;
        }

        var pkgList = document.getElementById('pkg-list');
        var pkgListItemTemplate = document.getElementById('pkg-list-item-template');
        
        var pkgPagesContainer = document.getElementById('pkg-pages');
        var pkgPageTemplate = document.getElementById('pkg-page-template');
        var pkgPageVersionTemplate = document.getElementById('pkg-page-version-template');
        var pkgPageFileItemTemplate = document.getElementById('pkg-page-file-item-template');

        // Loop through all packages we parsed from the index JSON.
        for (var i = 0; i < _cdnPackageIndex.packages.length; i++) {
            let pkg = _cdnPackageIndex.packages[i];

            // Add this package to the list on the main page.
            let pkgListItemClone = document.importNode(pkgListItemTemplate.content, true);
            
            let pkgListItemLink = pkgListItemClone.querySelector('a');
            if (pkgListItemLink !== null) {
                pkgListItemLink.setAttribute('href', '#' + pkg.package_id);
                pkgListItemLink.textContent = pkg.package_name;
            }
            let pkgListItemDesc = pkgListItemClone.querySelector('span');
            if (pkgListItemDesc !== null) {
                pkgListItemDesc.innerHTML = pkg.package_description_html;
            }
            pkgList.appendChild(pkgListItemClone);

            // Create a page for this package.
            let pkgPageClone = document.importNode(pkgPageTemplate.content, true);
            let pkgPageSection = pkgPageClone.querySelector('section.pkg');
            if (pkgPageSection !== null) {
                pkgPageSection.setAttribute('id', pkg.package_id + '-page');
            }
            let pkgPageTitle = pkgPageClone.querySelector('h2.pkg-name');
            if (pkgPageTitle !== null) {
                pkgPageTitle.textContent = pkg.package_name;
            }
            let pkgPageDesc = pkgPageClone.querySelector('p.pkg-desc');
            if (pkgPageDesc !== null) {
                pkgPageDesc.innerHTML = pkg.package_description_html;
            }

            // Create the dropdown list of versions for this package.
            let versionMenu = document.createElement('select');
            versionMenu.id = pkg.package_id + '-version';
            versionMenu.classList.add('pkg-version', 'padding-5', pkg.package_id);
            versionMenu.addEventListener('change', function(e) {
                _setActivePkgVersion(pkg.package_id);
            });
            
            // Create the list of files in each version.
            let allFileLists = new Array();
            for (var v = 0; v < pkg.package_versions.length; v++) {
                let vers = pkg.package_versions[v];
                
                // Create an option in the version dropdown for each version
                let option = document.createElement('option');
                option.text = vers.version;
                option.value = `${pkg.package_id}-${vers.version}`;
                if (v == pkg.package_versions.length - 1) {
                    option.setAttribute('selected', 'selected');
                }
                versionMenu.appendChild(option);

                // Create a list of files for each version
                let fileList = document.importNode(pkgPageVersionTemplate.content, true);
                let fileListElement = fileList.querySelector('ol');
                fileListElement.id = `${pkg.package_id}-${vers.version}`;
                fileListElement.classList.add(pkg.package_id);

                for (var f = 0; f < vers.files.length; f++) {
                    let file = _cdnBaseUri + vers.files[f];

                    let fileListItem = document.importNode(pkgPageFileItemTemplate.content, true);
                    fileListItem.querySelector('span.pkg-link-uri').textContent = file;
                    fileListElement.appendChild(fileListItem);
                }

                allFileLists.push(fileList);
            }
            
            pkgPageClone.querySelector('p.pkg-links').appendChild(versionMenu);

            for (var fl = 0; fl < allFileLists.length; fl++) {
                pkgPageSection.appendChild(allFileLists[fl]);
            }
            
            pkgPagesContainer.appendChild(pkgPageClone);
            _setActivePkgVersion(pkg.package_id);
        }

        // Print the last updated date.
        if (_cdnPackageIndex.last_updated) {
            let lastUpdated = new Date(_cdnPackageIndex.last_updated);
            document.getElementById('last-updated').innerText = `Last updated: ${lastUpdated.toLocaleDateString()} ${lastUpdated.toLocaleTimeString()}`;
        }
    }
    
    /**
     * Set up the page UI.
     */
    var _prepareUI = function () {
        _packageLinks = document.getElementsByClassName('pkg-list-name');
        _packageFileItems = document.getElementsByClassName('pkg-link-item');
        _initialPage = null;
        
        for (var i = 0; i < _packageLinks.length; i++) {
            // Set up click event on each package name
            _packageLinks[i].addEventListener('click', function(e) {
                e.preventDefault();
                let _package = e.currentTarget.getAttribute('href').replace('#', '');
                history.pushState(_package, null, '#' + _package);
                _showPage(_package);
            });

            // Check if a hash was supplied and go directly to that page
            if (window.location.hash && _packageLinks[i].getAttribute('href') === window.location.hash) {
                _initialPage = _packageLinks[i].getAttribute('href');
            }
        }

        // Push the initial state to the history API
        if (_initialPage !== null) {
            history.pushState(_initialPage, null, _initialPage);
            _showPage(_initialPage);
        }
        else {
            history.pushState('pkg-list', null, '#/');
        }

        // Set up click events that return to the bucket index
        document.getElementById('index-link').addEventListener('click', _backToIndex);
        document.getElementById('header-logo').addEventListener('click', _backToIndex);
        document.getElementById('header-title').addEventListener('click', _backToIndex);

        // Initialize each each file item in each package
        for (var i = 0; i < _packageFileItems.length; i++) {
            // Set up click event to select the file URI
            _packageFileItems[i].addEventListener('click', function(e) {
                let uriText = e.currentTarget.firstElementChild;
                _selectText(uriText);
            })
            // Add a "Copy" button to each file item and set up its click event
            let button = document.createElement('button');
            let buttonText = document.createTextNode('Copy');
            button.appendChild(buttonText);
            button.classList.add('font-body', 'rustler-red-bkg', 'rustler-gray-border', 'padding-5', 'padding-10-l', 'padding-10-r');
            button.addEventListener('click', function(e) {
                e.stopPropagation();
                let uriText = e.currentTarget.parentElement.firstElementChild;
                _selectText(uriText);
                _copySelectedText(uriText.innerText).then(function(succeeded) {
                    if (succeeded) {
                        _createPopupMessage('Copy succeeded!', 'success', false, 2000);
                    }
                    else {
                        _createPopupMessage('Copy failed. Try copying manually.', 'error', false);
                    }
                });
            });
            _packageFileItems[i].appendChild(button);
        }

        _initialized = true;
    }

    /**
     * Display a popup message.
     * @param {string} message The text of the message.
     * @param {string} type The type of popup to use (success|error).
     * @param {boolean} isModal If true, the message will appear centered and will block interaction until dismissed.
     * @param {number} autoCloseTime Milliseconds to wait before auto-closing the message. N/A with isModal=true.
     */
    var _createPopupMessage = function (message, type, isModal, autoCloseTime) {
        if (!message) return;
        let closeDelay = 4000;
        if (autoCloseTime !== undefined && parseInt(autoCloseTime))
            closeDelay = parseInt(autoCloseTime);
        
        let popupClone = document.importNode(document.getElementById('popup-message-template').content, true);
        let popup = popupClone.querySelector('div.popup-message');
        let button = popup.querySelector('button');
        popup.querySelector('.popup-text').textContent = message;

        if (type)
            popup.classList.add(type);
        
        if (isModal === true) {
            popupClone.querySelector('div.popup-container').classList.add('center');
            button.addEventListener('click', function(e) {
                e.currentTarget.parentElement.parentElement.remove();
            });
        }
        else {
            window.setTimeout(function() {
                popup.classList.add('in-view');
            }, 10);

            if (closeDelay > 0)
                window.setTimeout(function() {
                    popup.classList.remove('in-view');
                    window.setTimeout(function() {
                        popup.remove();
                    }, 300);
                }, closeDelay);
        }

        if (isModal === true) {
            document.getElementById('viewport').appendChild(popupClone);
            button.focus();
        }
        else {
            document.getElementById('viewport').appendChild(popup);
        }
        
    }

    /**
     * Sets which list of package files is visible depending on the active version.
     * @param {string} pkgId The ID of the package whose version you want to set.
     */
    var _setActivePkgVersion = function (pkgId) {
        if (pkgId) {
            let selectedVersion = document.getElementById(`${pkgId}-version`).value;
            let pkgFileLists = document.getElementById(`${pkgId}-page`).getElementsByTagName('ol');
            for (var i = 0; i < pkgFileLists.length; i++) {
                if (pkgFileLists[i].id === selectedVersion) {
                    pkgFileLists[i].classList.add('in-view', 'highlight-changes');
                }
                else {
                    pkgFileLists[i].classList.remove('in-view', 'highlight-changes');
                }
            }
        }
    }

    /**
     * Switch the view to the specified page.
     */
    var _showPage = function (pageName) {
        if (pageName) {
            pageName = pageName.replace('#', '');
            if (pageName.indexOf('-page') !== pageName.length - 5)
                pageName = pageName + '-page';
        }
        
        if (pageName === 'pkg-list-page') {
            // We're going to the main index
            let packagePages = document.getElementsByClassName('pkg');
            for (var i = 0; i < packagePages.length; i++) {
                packagePages[i].classList.remove('in-view');
            }
            document.getElementById('pkg-list-page').classList.add('in-view');
            document.getElementById('index-link').classList.remove('in-view');
        }
        else if (pageName && pageName !== '') {
            // We're going to a package page
            let page = document.getElementById(pageName);
            if (page !== null) {
                // Use a small delay to prevent animation failure
                // when going directly to a specific page at startup
                window.setTimeout(function() {
                    document.getElementById('pkg-list-page').classList.remove('in-view');
                    document.getElementById('index-link').classList.add('in-view');
                    page.classList.add('in-view');
                }, 10);
            }
        }
    }

    /**
     * Prepare to send us back to the index page.
     */
    var _backToIndex = function (event) {
        event.preventDefault();
        let packagePages = document.getElementsByClassName('pkg');
        for (var i = 0; i < packagePages.length; i++) {
            if (packagePages[i].classList.contains('in-view')) {
                history.pushState('pkg-list', null, '#/');
                break;
            }
        }
        _showPage('pkg-list');
    }

    /**
     * Select the text of the specified element.
     */
    var _selectText = function (element) {
        var sel, range;
        _clearSelection();
        if (window.getSelection && document.createRange) {
            sel = window.getSelection();
            if (sel.toString() == '') {
                window.setTimeout(function() {
                    range = document.createRange();
                    range.selectNodeContents(element);
                    sel.addRange(range);
                }, 1);
            }
        }
        else if (document.selection) {
            sel = document.selection.createRange();
            if (sel.text == '') {
                range = document.body.createTextRange();
                range.moveToElementText(element);
                range.select();
            }
        }
    }

    /**
     * Clear any existing selection.
     */
    var _clearSelection = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection;
        if (selection) {
            if (selection.removeAllRanges) {
                selection.removeAllRanges();
            } else if (selection.empty) {
                selection.empty();
            }
        }
    }

    /**
     * Copy either the specified or currently selected text to the clipboard.
     */
    var _copySelectedText = async function (text) {
        var succeeded = false;
        if (navigator.clipboard && navigator.clipboard.writeText) {
            await navigator.clipboard.writeText(text).then(function() {
                succeeded = true;
            }, function () {
                succeeded = false;
            });
        }
        else {
            window.setTimeout(function() {
                document.execCommand('copy');
            }, 100);
            succeeded = true;
        }
        return succeeded;
    }

    /**
     * Set the visible state of a DOM element by its ID.
     * @param {string} id The ID of the element.
     * @param {boolean} state True to show it, false to hide it.
     */
    var _setElementVisibility = function (id, state) {
        var element = document.getElementById(id);
        if (element !== null) {
            if (state === false) {
                element.classList.remove('in-view');
            }
            else {
                element.classList.add('in-view');
            }
        }
    }

    /**
     * Show or hide the shadow backdrop.
     * @param {boolean} state True to show it, false to hide it.
     */
    var _showShadowBackdrop = function (state) {
        _setElementVisibility('shadow-backdrop', state);
    }

    /**
     * Show or hide the loading animation and shadow backdrop.
     * @param {Boolean} state True to show them, false to hide them.
     */
    var _showLoadingAnim = function (state) {
        _setElementVisibility('shadow-backdrop', state);
        _setElementVisibility('loading-anim', state);
    }

    /**
     * Returns whether the shadow backdrop is currently visible or not.
     */
    var _isShadowBackdropVisible = function () {
        var visible = false;
        var backdrop = document.getElementById('shadow-backdrop');
        if (backdrop !== null && backdrop.classList.contains('in-view') && backdrop.style.display !== 'none') {
            visible = true;
        }
        return visible;
    }

    /**
     * Initializes the module.
     */
    var _init = function () {
        
        if (_initialized === true) {
            console.log('Nothing to do. UI is already initialized.');
        }
        else {
            _loadPackageJson();
        }
    };
    
    // Reveal public methods.
    module.init = _init;
    module.showPage = _showPage;
    module.showShadowBackdrop = _showShadowBackdrop;
    module.showLoadingAnim = _showLoadingAnim;
    module.isShadowBackdropVisible = _isShadowBackdropVisible;

    // Return the module.
    return module;
  
})();

// Initialize the page at startup.
document.addEventListener('DOMContentLoaded', () => {
    CdnIndexModule.init();
});

// When back or forward is used, show the last page in the history state.
window.addEventListener('popstate', (event) => {
    if (event.state !== null)
        CdnIndexModule.showPage(event.state);
});